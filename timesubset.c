#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <unistd.h>

#define ESTR(x)	strerror(x)

unsigned long strtotimealign(const char *str, unsigned long ref)
{
	char *endp;
	long value, factor = 1;

	value = strtol(str, &endp, 10);
	switch (*endp) {
	case 'w':
		factor *= 7;
	case 'd':
		factor *= 24;
	case 'h':
		factor *= 60;
	case 'm':
		factor *= 60;
		break;
	case 'y':
		factor *= 365*86400;
		break;
	}

	if (value < 0) {
		ref = time(NULL);

		/* align ref */
		ref = ref - ref % factor;
	}
	return ref + value * factor;
}

static long tzoffset(time_t t)
{
	char buf[32];
	int offs;

	strftime(buf, sizeof(buf), "%z", localtime(&t));
	offs = strtoul(buf, NULL, 10);
	return (offs / 100) *3600 + (offs % 100) * 60;
}

int main(int argc, char *argv[])
{
	char *line = NULL;
	size_t linesize = 0;
	int ret;
	unsigned long tsample, tmin, tmax, tlast = 0;
	char *lastknown = NULL, *endp;

	tmin = 0;
	tmax = 0;

	if (argc > 1)
		tmin = strtotimealign(argv[1], 0);
	else {
		fprintf(stderr, "usage: %s MIN [MAX]\n", argv[0]);
		goto failed;
	}
	if (argc > 2 && strcmp(argv[2], "-"))
		tmax = strtotimealign(argv[2], tmin);

	if (isatty(2))
		fprintf(stderr, "select %lu..%lu, now %llu\n", tmin, tmax, (long long)time(NULL));

	for (;;) {
		ret = getline(&line, &linesize, stdin);
		if (ret < 0) {
			if (!feof(stdin))
				goto failed;
		}
		if (ret <= 0)
			break;
		tsample = strtoul(line, &endp, 10);
		if (tsample >= tmin && (!tmax || tsample <= tmax)) {
			if (lastknown && tmin && tlast < tmin)
				/* entered the time window */
				printf("%lu %lu%s\n", tzoffset(tmin), tmin, lastknown);
			printf("%lu ", tzoffset(tsample));
			fputs(line, stdout);
		} else if (tmax && tsample > tmax) {
			if (lastknown && tmin && tlast < tmin)
				printf("%lu %lu%s\n", tzoffset(tmin), tmin, lastknown);
			printf("%lu %lu%s\n", tzoffset(tmax), tmax, endp);
			return 0;
		}
		if (lastknown)
			free(lastknown);
		lastknown = strdup(endp);
		tlast = tsample;
	}
	if (tlast < tmin && lastknown)
		/* print single start point */
		printf("%lu %lu%s\n", tzoffset(tmax), tmin, lastknown);
	else if (tmax && tlast < tmax && lastknown) /* print end point */
		printf("%lu %lu%s\n", tzoffset(tmax), tmax, lastknown);
	return 0;
failed:
	return 1;
}
