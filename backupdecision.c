#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <syslog.h>
#include <wordexp.h>
#include <sys/uio.h>
#include <sys/stat.h>

#define NAME "backupdecision"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* program options */
static const char help_msg[] =
	NAME ": read timestamps from stdin, and decide which ones to keep\n"
	"	this tool only makes the decision, it should integrate\n"
	"	in a backup system\n"
	"usage:	" NAME " [OPTIONS ...] [-i FILE]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -i, --input=FILE	read from FILE\n"
	"\n"
	" -p, --pool=NAME,INTERVAL,COUNT	define new pool in backup strategy\n"
	"			INTERVAL has suffixes h, d, w or m\n"
	" -l, --level=NAME,NAME,...	Level between devices\n"
	"			Give all devices as comma-seperated list\n"
	"			The input expect a second field, with device names\n"
	" -t, --testtime=YYYYmmddTHHMMSS act as on the given timestamp\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "input", required_argument, NULL, 'i', },
	{ "pool", required_argument, NULL, 'p', },
	{ "level", required_argument, NULL, 'l', },
	{ "testtime", required_argument, NULL, 't', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "+Vv?h:f:p:l:t:";

/* logging */
static int max_loglevel = LOG_WARNING;
#define LOG_EXIT	0x4000000

/* safeguard our LOG_EXIT extension */
#if (LOG_EXIT & (LOG_FACMASK | LOG_PRIMASK))
#error LOG_EXIT conflict
#endif

__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	static int logtostderr = -1;
	va_list va;
	char *str;
	int loglevel2 = loglevel & ~(LOG_FACMASK | LOG_PRIMASK);
	loglevel &= LOG_FACMASK | LOG_PRIMASK;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(NAME, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel, "%s", str);

	} else if (loglevel <= max_loglevel) {
		struct timespec tv;
		static char timbuf[64];

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
	}

	free(str);
	if (loglevel2 & LOG_EXIT)
		exit(1);
}
#define ESTR(x) strerror(x)

/* parameters */
static char *inputfilename;
static time_t now;

struct pool {
	char *name;
	int interval;
	int max;
	time_t lastdone;
	time_t discardtime;
};

static struct pool *pools;
static int maxpools, npools;

struct device;
struct entry {
	time_t time;
	struct device *dev;
	struct pool *pool;
};
static struct entry *entries;
static int maxentries, nentries;

struct device {
	char *name;
	time_t lastkeep;
	time_t lastany;
};

static struct device *devices;
static int maxdevices, ndevices;

/* program */
static long delay_strtol(const char *str, char **pendp, int radix)
{
	char *endp;
	long result;

	if (!pendp)
		pendp = &endp;
	result = strtol(str, pendp, radix);
	switch (**pendp) {
	case 'y':
		result *= 365*24*60*60;
		++*endp;
		break;
	case 'w':
		result *= 7;
	case 'd':
		result *= 24;
	case 'h':
		result *= 60;
	case 'm':
		result *= 60;
	case 's':
		++*pendp;
		break;
	}
	return result;
}

static int cmp_pool(const void *va, const void *vb)
{
	const struct pool *pa = va;
	const struct pool *pb = vb;

	return pb->interval - pa->interval;
}
static int cmp_entry(const void *va, const void *vb)
{
	const struct entry *ea = va;
	const struct entry *eb = vb;

	return ea->time - eb->time;
}
static int cmp_device(const void *va, const void *vb)
{
	const struct device *a = va;
	const struct device *b = vb;

	if (a->lastkeep != b->lastkeep)
		return a->lastkeep - b->lastkeep;
	return a->lastany - b->lastany;
}

static const char *ttostr(time_t t)
{
	static char tstr[128];

	strftime(tstr, sizeof(tstr), "%Y%m%dT%H%M%S", localtime(&t));
	return tstr;
}
static time_t strtot(const char *str)
{
	struct tm tm = {};
	char *end;

	end = strptime(str, "%Y%m%dT%H%M%S", &tm);
	if (end <= str)
		return 0;
	return mktime(&tm);
}

static struct device *lookup_device(const char *device)
{
	int j;

	for (j = 0; j < ndevices; ++j) {
		if (!strcmp(device, devices[j].name))
			return devices+j;
	}
	return NULL;
}

static void notify(char action, const char *pool, time_t t, struct device *dev)
{
	printf("%c%.1s %s", action, pool ?: "", ttostr(t));
	if (ndevices) {
		if (dev)
			printf(" %s", dev->name);
		else
			printf(" -");
	}
	printf("\n");
}

int main(int argc, char *argv[])
{
	int ret, opt;
	int j;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s (%s %s)\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		max_loglevel += 1;
		break;
	case 'i':
		inputfilename = optarg;
		break;

	case 'p':
		if (npools >= maxpools) {
			/* allocate more pools */
			maxpools += 16;
			pools = realloc(pools, maxpools*sizeof(*pools));
			if (!pools)
				mylog(LOG_ERR | LOG_EXIT, "realloc %u pools: %s", maxpools, ESTR(errno));
		}
		for (optarg = strtok(optarg, ",;"), j = 0; optarg; optarg = strtok(NULL, ",;"), ++j)
		switch (j) {
		case 0:
			memset(pools+npools, 0, sizeof(*pools));
			pools[npools].name = optarg;
			break;
		case 1:
			pools[npools].interval = delay_strtol(optarg, NULL, 0);
			break;
		case 2:
			pools[npools++].max = strtoul(optarg, NULL, 0);
			break;
		}
		break;
	case 'l':
		for (optarg = strtok(optarg, ",;"); optarg; optarg = strtok(NULL, ",;")) {
			if (lookup_device(optarg))
				/* exist already */
				continue;
			/* append */
			if (ndevices >= maxdevices) {
				maxdevices += 16;
				devices = realloc(devices, maxdevices * sizeof(*devices));
				if (!devices)
					mylog(LOG_ERR | LOG_EXIT, "realloc %u devices: %s", maxdevices, ESTR(errno));
			}
			memset(devices+ndevices, 0, sizeof(*devices));
			devices[ndevices].name = optarg;
			++ndevices;
		}
		break;
	case 't':
		now = strtot(optarg);
		if (!now)
			mylog(LOG_WARNING, "bad time '%s'", optarg);
		break;
	default:
		mylog(LOG_WARNING, "unknown option '%c'", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;
	}

	if (!npools)
		mylog(LOG_ERR | LOG_EXIT, "no pools defined");

	if (inputfilename) {
		int fd;

		fd = open(inputfilename, O_RDONLY);
		if (fd < 0)
			mylog(LOG_ERR | LOG_EXIT, "open %s: %s", inputfilename, ESTR(errno));
		dup2(fd, STDIN_FILENO);
		close(fd);
	} else {
		inputfilename = "stdin";
	}

	char *line = NULL;
	size_t linesiz = 0;
	time_t t;
	char *tim, *dev;

	for (;;) {
		ret = getline(&line, &linesiz, stdin);
		if (ret < 0 && feof(stdin))
			break;
		if (ret < 0)
			mylog(LOG_ERR | LOG_EXIT, "getline %s: %s", inputfilename, ESTR(errno));
		if (!ret)
			break;
		tim = strtok(line, " \t\r\n");
		dev = strtok(NULL, " \t\r\n");
		/* parse */
		t = strtot(tim);
		if (!t) {
			mylog(LOG_ERR | LOG_EXIT, "bad format '%s'", tim);
			continue;
		}
		/* append */
		if (nentries >= maxentries) {
			maxentries = maxentries*2 ?: 16;
			entries = realloc(entries, maxentries * sizeof(*entries));
			if (!entries)
				mylog(LOG_ERR | LOG_EXIT, "realloc %i entries: %s", maxentries, ESTR(errno));
		}
		memset(entries+nentries, 0, sizeof(*entries));
		entries[nentries].time = t;
		entries[nentries].dev = lookup_device(dev);
		++nentries;
	}
	fclose(stdin);

	/* sort pools desc, entries asc */
	qsort(pools, npools, sizeof(*pools), cmp_pool);
	qsort(entries, nentries, sizeof(*entries), cmp_entry);

	struct pool *pool;
	struct entry *curr;
	int added;

	if (!now)
		time(&now);

	for (pool = pools; pool < pools+npools; ++pool) {
		pool->discardtime = now - pool->interval * pool->max;
		mylog(LOG_INFO, "pool %s, i %u, n %u, since %s",
				pool->name, pool->interval, pool->max,
				ttostr(pool->discardtime));
	}
	mylog(LOG_INFO, "status on %s", ttostr(now));

	for (curr = entries; curr < entries+nentries; ++curr) {
		added = 0;
		for (pool = pools; pool < pools+npools; ++pool) {
			if (curr->time < pool->discardtime)
				continue;
			if (added) {
				pool->lastdone = curr->time;
			} else if (curr->time >= pool->lastdone + pool->interval - 60) {
				/* take 60sec margin for decisions */
				notify('=', pool->name, curr->time, curr->dev);
				pool->lastdone = curr->time;
				added = 1;
				curr->pool = pool;
				/* entries is sorted correctly, no need to check */
				if (curr->dev)
					curr->dev->lastkeep = curr->time;
			}
		}
		if (curr->dev)
			curr->dev->lastany = curr->time;
		if (!added)
			notify('-', NULL, curr->time, curr->dev);
	}
	/* sort devices asc */
	qsort(devices, ndevices, sizeof(*devices), cmp_device);
	for (pool = pools; pool < pools+npools; ++pool) {
		if (pool->lastdone + pool->interval - 60 < now) {
			mylog(LOG_INFO, "pool %s needs new", pool->name);
			/* select oldest 'last' backup device */
			notify('+', pool->name, now, devices);
			break;
		}
	}

	return 0;
}
