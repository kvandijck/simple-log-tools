#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <getopt.h>
#include <syslog.h>
#include <fcntl.h>
#include <sys/uio.h>
#include <sys/wait.h>

#define NAME "maxtime"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* program options */
static const char help_msg[] =
	NAME ": run child for max wall time\n"
	"usage:	" NAME " [OPTIONS ...] -t DELAY COMMAND [ARGS ...]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"

	" -e, --stderr		Log to stderr, no probe for tty or syslog\n"
	" -t, --delay=DELAY	max delay before killing child\n"
	"			suffixes s,m,h,w allowed\n"
	" -9, --hard=DELAY	delay before hard-killing child after normal kill\n"
	"			(default 5s)\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "stderr", no_argument, NULL, 'e', },
	{ "delay", required_argument, NULL, 't', },
	{ "hard", required_argument, NULL, '9', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "+Vv?et:9:";

static int termdelay;
static int harddelay = 5;
static int childpid;

/* logging */
static int max_loglevel = LOG_WARNING;
#define LOG_EXIT	0x4000000

/* safeguard our LOG_EXIT extension */
#if (LOG_EXIT & (LOG_FACMASK | LOG_PRIMASK))
#error LOG_EXIT conflict
#endif

static int logtostderr = -1;
__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	va_list va;
	char *str;
	int loglevel2 = loglevel & ~(LOG_FACMASK | LOG_PRIMASK);
	loglevel &= LOG_FACMASK | LOG_PRIMASK;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(NAME, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel, "%s", str);

	} else if (loglevel <= max_loglevel) {
		struct timespec tv;
		static char timbuf[64];

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
	}

	free(str);
	if (loglevel2 & LOG_EXIT)
		exit(1);
}
#define ESTR(x) strerror(x)

/* program */
static double delay_strtod(const char *str, char **pendp)
{
	char *endp;
	double result;

	if (!pendp)
		pendp = &endp;
	result = strtod(str, pendp);
	switch (**pendp) {
	case 'y':
		result *= 365*24*60*60;
		++*endp;
		break;
	case 'w':
		result *= 7;
	case 'd':
		result *= 24;
	case 'h':
		result *= 60;
	case 'm':
		result *= 60;
	case 's':
		++*pendp;
		break;
	}
	return result;
}

static volatile int sigalrm;
static void sighandler(int sig)
{
	switch (sig) {
	case SIGALRM:
		sig = sigalrm++ ? SIGKILL : SIGTERM;
		mylog(LOG_NOTICE, "kill %i %i", childpid, sig);
		if (kill(childpid, sig) < 0)
			mylog(LOG_WARNING, "kill %i %i: %s", childpid, sig, ESTR(errno));
		signal(SIGALRM, sighandler);
		alarm(harddelay);
		break;
	case SIGINT:
	case SIGTERM:
		/* forward signal to child */
		mylog(LOG_NOTICE, "kill %i %i", childpid, sig);
		if (kill(childpid, sig) < 0)
			mylog(LOG_WARNING, "kill %i %i: %s", childpid, sig, ESTR(errno));
		signal(sig, sighandler);
		++sigalrm;
		alarm(harddelay);
		break;
	}
}

int main(int argc, char *argv[])
{
	int ret, opt;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s (%s %s)\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		max_loglevel += 1;
		break;
	case 'e':
		logtostderr = 1;
		break;
	case 't':
		termdelay = delay_strtod(optarg, NULL);
		break;
	case '9':
		harddelay = delay_strtod(optarg, NULL);
		break;
	default:
		fprintf(stderr, "unknown option '%c'", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;
	}

	if (!termdelay)
		mylog(LOG_ERR | LOG_EXIT, "Specify either -t");

	if (optind >= argc)
		mylog(LOG_ERR | LOG_EXIT, "no child program specified");

	/* spawn child process */
	childpid = ret = fork();
	if (ret < 0)
		mylog(LOG_EXIT | LOG_ERR, "fork: %s", ESTR(errno));
	if (ret == 0) {
		/* parent, replace with target program
		 * This is less conventional,
		 * but makes brsimplelog a utility rather than
		 * the core tool.
		 * The process' exit code is that of the monitored program.
		 */
		execvp(argv[optind], argv+optind);
		mylog(LOG_EXIT | LOG_ERR, "execvp %s ...: %s", argv[optind], ESTR(errno));
	}
	mylog(LOG_NOTICE, "child %i ...", childpid);

	signal(SIGTERM, sighandler);
	signal(SIGINT, sighandler);
	signal(SIGALRM, sighandler);
	alarm(termdelay);

	int status;
	do {
		ret = wait(&status);
	} while (ret < 0 && errno == EINTR);

	if (ret < 0) {
		mylog(LOG_ERR, "wait: %s", ESTR(errno));
		kill(childpid, SIGTERM);
		/* waiting for real exit here does not make sense,
		 * we can't, so we got here */
		exit(1);
	}
	/* propagate exit status */
	if (WIFEXITED(status))
		ret = WEXITSTATUS(status);
	else if (WIFSIGNALED(status))
		ret = 128+WTERMSIG(status);
	else
		ret = 1;
	mylog(LOG_NOTICE, "child %i returned: %i", childpid, ret);
	return ret;
}
