#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <fnmatch.h>
#include <locale.h>
#include <wordexp.h>
#include <syslog.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/uio.h>

#define NAME "mqttlogquery"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

static const char help_msg[] =
	NAME ": find averages, delta, max of time-series data\n"
	"usage:	" NAME " [OPTIONS ...] [FILE ...]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -e, --stderr		Log sto stderr unconditionally\n"
	"\n"
	" -t, --topic=TOPIC[,DELAY][,avg=INTERVAL]	filter TOPIC\n"
	"			DELAY for delayed values (totals at the end of the interval)\n"
	" -0, --t0=(-DELAY(m|h|w)|TIME)\n"
	" -1, --te=DELAY(m|h|w)\n"
	" -z, --tz		add timezone-offset column\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },
	{ "stderr", no_argument, NULL, 'e', },

	{ "topic", required_argument, NULL, 't', },
	{ "t0", required_argument, NULL, '0', },
	{ "te", required_argument, NULL, '1', },
	{ "tz", optional_argument, NULL, '1', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vve?h:t:0:1:z";

/* logging */
static int max_loglevel = LOG_WARNING;
#define LOG_EXIT	0x4000000
/* safeguard our LOG_EXIT extension */
#if (LOG_EXIT & (LOG_FACMASK | LOG_PRIMASK))
#error LOG_EXIT conflict
#endif

static int logtostderr = -1;
__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	va_list va;
	char *str;
	int loglevel2 = loglevel & ~(LOG_FACMASK | LOG_PRIMASK);
	loglevel &= LOG_FACMASK | LOG_PRIMASK;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(NAME, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel, "%s", str);

	} else if (loglevel <= max_loglevel) {
		struct timespec tv;
		static char timbuf[64];

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
	}

	free(str);
	if (loglevel2 & LOG_EXIT)
		exit(1);
}
#define ESTR(x) strerror(x)

/* program options */
static time_t tmin, tmax;
static int with_tz;
static double max_delay = 0;

/* data */
struct sample {
	double t;
	double v;
};
struct item {
	char *topic;
	int topiclen;
	double delay;
	double mul;
	double value;
	int flags;
		#define FL_AVG	(1 << 0)
	double avginterval;
	double nextinterval;
	int newval;
	struct sample *samples;
	int nsamples;
	int ssamples;
	int fill;
	int pos;
};

static struct item *items;
static int nitems;
static int sitems;

/* code */
static struct item *add_item(const char *topic)
{
	struct item *it;
	char *tok, *val;

	if (nitems >= sitems) {
		sitems = (sitems ?: 16) * 2;
		items = realloc(items, sizeof(*items)*sitems);
		if (!items)
			mylog(LOG_ERR | LOG_EXIT, "malloc %i items: %s", sitems, ESTR(errno));
	}
	it = &items[nitems++];
	memset(it, 0, sizeof(*it));
	it->topic = strdup(topic);
	it->delay = 0;
	it->value = 0;
	it->mul = 1;
	for (tok = strtok(it->topic, ",;"); tok; tok = strtok(NULL, ",;")) {
		if (tok == it->topic)
			/* this is the main topic, skip */
			continue;
		val = strchr(tok, '=');
		if (val)
			*val++ = 0;
		if (!strcmp(tok, "delay")) {
			it->delay = strtod(val, NULL);
			if (max_delay < it->delay)
				max_delay = it->delay;
		} else if (!strcmp(tok, "avg")) {
			it->flags |= FL_AVG;
			it->avginterval = strtod(val ?: "900", NULL);
		} else if (!strcmp(tok, "mul")) {
			it->mul = strtod(val ?: "1", NULL);
		}
	}
	it->topiclen = strlen(it->topic);
	mylog(LOG_INFO, "* %s", topic);
	return it;
}

static struct item *find_item2(const char *topic, int len)
{
	struct item *it;

	for (it = items; it < items+nitems; ++it) {
		if (it->topiclen != len)
			continue;
		if (!strncmp(it->topic, topic, len))
			return it;
	}
	return NULL;
}

unsigned long strtotimealign(const char *str, unsigned long ref)
{
	char *endp;
	long value, factor = 1;

	value = strtol(str, &endp, 10);
	switch (*endp) {
	case 'w':
		factor *= 7;
	case 'd':
		factor *= 24;
	case 'h':
		factor *= 60;
	case 'm':
		factor *= 60;
		break;
	case 'y':
		factor *= 365*86400;
		break;
	}

	if (value < 0) {
		ref = time(NULL);

		/* align ref */
		ref = ref - ref % factor;
	}
	return ref + value * factor;
}

static long tzoffset(time_t t)
{
	char buf[32];
	int offs;

	strftime(buf, sizeof(buf), "%z", localtime(&t));
	offs = strtoul(buf, NULL, 10);
	return (offs / 100) *3600 + (offs % 100) * 60;
}

/* output */
static double currt0 = NAN, currte;
static double currtsum;
static int currntsum;

static void dump(void)
{
	struct item *it;

	printf("%.3f", currtsum/currntsum);
	if (with_tz)
		printf(" %lu", tzoffset(currtsum/currntsum));
	for (it = items; it < items+nitems; ++it) {
		printf(" %f", it->value);
		it->newval = 0;
	}
	printf("\n");
}

static void item_avg(double tim, struct item *it)
{
	if (!it->nsamples) {
		it->nextinterval = ceil((tim-it->delay)/it->avginterval)*it->avginterval + it->delay;
		return;
	}
	/* no new samples since last avg */
	if (it->fill >= it->nsamples)
		return;

	if (tim < it->nextinterval)
		return;

	double timref = it->nextinterval;
	it->nextinterval = ceil((tim-it->delay)/it->avginterval)*it->avginterval + it->delay;

	int j;
	double vsum, tsum, delay;
	for (j = it->fill+1, vsum = tsum = 0; j < it->nsamples; ++j) {
		delay = it->samples[j].t - it->samples[j-1].t;
		tsum += delay;
		vsum += it->samples[j-1].v * delay;
	}
	delay = timref - it->samples[j-1].t;
	tsum += delay;
	vsum += it->samples[j-1].v * delay;

	/* prepare new last sample */
	it->samples[it->fill+1].t = timref;
	it->samples[it->fill+1].v = it->samples[it->nsamples-1].v;
	/* commit */
	it->samples[it->fill].v = vsum/tsum;
	++it->fill;
	it->nsamples = it->fill+1;
}

static void collect(double tim, struct item *it, double value)
{
	/* +1, leave room for avg */
	if (it->nsamples+1 >= it->ssamples) {
		it->ssamples = (it->ssamples ?: 16)*2;
		it->samples = realloc(it->samples, sizeof(it->samples[0])*it->ssamples);
		if (!it->samples)
			mylog(LOG_ERR | LOG_EXIT, "realloc %i samples: %s", it->ssamples, ESTR(errno));
	}
	if (it->flags & FL_AVG)
		item_avg(tim, it);
	it->samples[it->nsamples].t = tim;
	it->samples[it->nsamples].v = value;
	it->nsamples += 1;
}

static void process(struct item *it, double tim, double value)
{
	if (it->newval || tim - currt0 > 1 || tim - currte > 0.5) {
		if (currntsum)
			dump();
		currntsum = 1;
		currt0 = currte = currtsum = tim;
	}
	currte = tim;
	it->value = value;
	it->newval = 1;
}

static void consume(void)
{
	struct item *sel;
	struct item *it;

	for (it = items; it < items+nitems; ++it) {
		if (it->nsamples && it->flags & FL_AVG)
			item_avg(it->samples[it->nsamples-1].t+it->avginterval, it);
		it->value = NAN;
	}

	for (;;) {
		for (sel = NULL, it = items; it < items+nitems; ++it) {
			if (it->pos >= it->nsamples)
				continue;
			else if (!sel)
				sel = it;
			else if ((it->samples[it->pos].t - it->delay) < (sel->samples[sel->pos].t - sel->delay))
				sel = it;
		}
		if (!sel)
			break;
		process(sel, sel->samples[sel->pos].t - sel->delay, sel->samples[sel->pos].v);
		++sel->pos;
		if (it->pos >= it->nsamples)
			it->value = NAN;
	}
	if (sel)
		dump();
}

__attribute__((unused))
static double consume_time(const char *str, char **endp)
{
#if 0
	return strtod(str, endp);
#else
	char *myendp;

	/* assure endp is set */
	if (!endp)
		endp = &myendp;

	double result = strtol(str, endp, 10);

	if (**endp == '.') {
		unsigned long dec = strtoul((*endp)+1, endp, 10);

		result += dec*10e-9;
	}
	return result;
#endif
}
__attribute__((unused))
static struct item *consume_topic(const char *str, char **endp)
{
	*endp = strpbrk(str, " \t\r\n");
	if (!*endp)
		return NULL;
	return find_item2(str, *endp-str);
}

static double tim;
static void line_recvd(char *line, int len)
{
	double value;
	struct item *it;
	char *endp;

	/* only now convert time */
	tim = consume_time(line, &endp);
	if (tim < tmin || tim > (tmax + max_delay))
		return;

	it = consume_topic(endp+1, &endp);
	if (!it)
		return;
	if (*endp == ' ')
		value = strtod(endp+1, NULL);
	else
		value = 0;

	if ((tim-it->delay) < tmin || (tim-it->delay) > tmax)
		return;
	collect(tim, it, value*it->mul);
}

static void do_file(int fd, const char *filename)
{
	struct stat st;
	char *addr;
	char *line, *eol;

	if (fstat(fd, &st))
		mylog(LOG_ERR | LOG_EXIT, "fstat %s: %s", filename, ESTR(errno));
	if (st.st_mtime < tmin)
		/* skip this file */
		return;
	addr = mmap(NULL, st.st_size, PROT_READ|PROT_WRITE, MAP_PRIVATE, fd, 0);
	if (addr == MAP_FAILED)
		mylog(LOG_ERR | LOG_EXIT, "mmap %s: %s", filename, ESTR(errno));

	for (line = addr; line < addr+st.st_size; line = eol+1) {
		eol = strchr(line, '\n');
		if (!eol)
			break;
		line_recvd(line, eol-line);
		if (line == addr) {
			/* first line */
			if (tim > tmax)
				break;
		}
	}
	munmap(addr, st.st_size);
}

static const char *const we_err[] = {
	[WRDE_BADCHAR] = "Illegal occurrence of newline or one of |, &, ;, <, >, (, ), {, }",
	[WRDE_BADVAL] = "An undefined shell variable was referenced, with WRDE_UNDEF",
	[WRDE_CMDSUB] = "Command substitution occurred with WRDE_NOCMD",
	[WRDE_NOSPACE] = "Out of memory",
	[WRDE_SYNTAX] = "Shell syntax error, such as unbalanced parentheses or unmatched quotes",
};

static int cmpfilename(const void *a, const void *b)
{
	const char **fa = (const char **)a;
	const char **fb = (const char **)b;
	return strcmp(*fa, *fb);
}

int main(int argc, char *argv[])
{
	if (!setlocale(LC_ALL, ""))
		mylog(LOG_ERR | LOG_EXIT, "setlocale failed: %s", ESTR(errno));

	/* argument parsing */
	int opt;
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s (%s %s)\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		max_loglevel += 1;
		break;
	case 'e':
		logtostderr = 1;
		break;
	default:
		mylog(LOG_WARNING, "unknown option '%c'", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;

	case 't':
		add_item(optarg);
		break;
	case '0':
		tmin = strtotimealign(optarg, 0);
		break;
	case '1':
		tmax = strtotimealign(optarg, tmin);
		break;
	case 'z':
		with_tz = 1;
		break;
	}

	if (!tmax)
		tmax = time(NULL);
	const char *filename;
	const char *pattern;
	int fd, j, ret;

	for (; optind < argc; ++optind) {
		pattern = argv[optind];

		if (!strcmp("-", pattern)) {
			do_file(STDIN_FILENO, filename);
		} else {
			wordexp_t we = {};
			ret = wordexp(pattern, &we, WRDE_NOCMD | WRDE_REUSE);
			if (ret)
				mylog(LOG_ERR | LOG_EXIT, "'%s' failed: %s", pattern, we_err[ret] ?: "unknown error");
			if (we.we_wordc == 1 && stat(we.we_wordv[0], &(struct stat){}) != 0) {
				/* no match */
				mylog(LOG_NOTICE, "pattern '%s' matched no file", pattern);
				continue;
			}
			qsort(we.we_wordv, we.we_wordc, sizeof(*we.we_wordv), cmpfilename);
			for (j = 0; j < we.we_wordc; ++j) {
				filename = we.we_wordv[j];
				struct stat st;
				if (stat(filename, &st))
					mylog(LOG_ERR | LOG_EXIT, "stat %s: %s", filename, ESTR(errno));
				if (st.st_mtime < tmin)
					/* skip this file */
					continue;
				fd = open(filename, O_RDONLY);
				if (fd < 0)
					mylog(LOG_ERR | LOG_EXIT, "open %s: %s", filename, ESTR(errno));
				do_file(fd, filename);
				close(fd);
			}
			wordfree(&we);
		}
	}
	consume();
	return 0;
}
