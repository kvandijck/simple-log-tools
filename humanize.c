#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>

#define die(fmt, ...)	({ fprintf(stderr, (fmt), ##__VA_ARGS__); fflush(stderr); exit(1); })
#define ESTR(x)	strerror(x)


int main(int argc, char *argv[])
{
	char *line = NULL, *endp;
	size_t linesize = 0;
	int len, fd;
	time_t stmp;
	char tbuf[128];

	if (argv[1]) {
		fd = open(argv[1], O_RDONLY);
		if (fd < 0)
			die("open %s: %s", argv[1], ESTR(errno));
		if (dup2(fd, STDIN_FILENO) < 0)
			die("dup2: %s", ESTR(errno));
		close(fd);
	}

	for (;;) {
		len = getline(&line, &linesize, stdin);
		if (len < 0 && feof(stdin))
			break;
		if (len < 0)
			die("read: %s", ESTR(errno));
		line[len] = 0;

		stmp = strtoul(line, &endp, 10);
		strftime(tbuf, sizeof(tbuf), "%Y%m%dT%H%M%S", localtime(&stmp));
		printf("%s%s", tbuf, endp);
		fflush(stdout);
	}
	return 0;
}
