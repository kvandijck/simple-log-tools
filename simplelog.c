#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <syslog.h>
#include <wordexp.h>
#include <sys/uio.h>
#include <sys/stat.h>

#define NAME "simplelog"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* program options */
static const char help_msg[] =
	NAME ": save stdin to file and rotate\n"
	"usage:	" NAME " [OPTIONS ...] -f FILE COMMAND [ARGS ...]\n"
	"	" NAME " [OPTIONS ...] -f FILE\n"
	"	" NAME " [OPTIONS ...] -L FILE [...]\n"
	"	" NAME " [OPTIONS ...] -C PATTERN [...]\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -L, --logrotate	Only logrotate\n"
	" -C, --clean		Only clean, works on PATTERN directly!\n"
	" -c, --cmp=ITEM	Compare using (n)ame, (m)time or (a)time\n"
	" -n, --dryrun		No action (for -L and -C)\n"
	" -f, --file=FILE	save into FILE\n"
	" -s, --size=FILESIZE	Rotate files when they grown beyond FILESIZE (default 5MB)\n"
	" -t, --time=DELAY	Rotate files at least after DELAY (default 1 day)\n"
	" -S, --accsize=SIZE	Remove files from disk when they accumulate beyond SIZE (default 50MB)\n"
	" -T, --maxage=DELAY	Remove files from disk when they're older than DELAY (default 1 week)\n"
	" -p, --parent		remain parent (child exit status will be lost)\n"
	" -2, --stderr		Log stderr too\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "file", required_argument, NULL, 'r', },
	{ "size", required_argument, NULL, 's', },
	{ "time", required_argument, NULL, 't', },
	{ "accsize", required_argument, NULL, 'S', },
	{ "maxage", required_argument, NULL, 'T', },
	{ "stderr", no_argument, NULL, '2', },
	{ "parent", no_argument, NULL, 'p', },

	{ "logrotate", no_argument, NULL, 'L', },
	{ "clean", no_argument, NULL, 'C', },
	{ "cmp", required_argument, NULL, 'c', },
	{ "dryrun", no_argument, NULL, 'n', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "+Vv?h:f:s:t:S:T:2pLCc:n";

#define MB	*1024*1024
#define Hr	*3600
#define Day	*86400
static char *filename;
static char *filepattern;
static long maxsize = 5 MB;
static long maxaccsize = 50 MB;
static double maxdelay = 1 Day;
static double maxage = 7 Day;

static int logrotate;
static int vacuum;
static int dryrun;
static int (*cmpfile)(const void *, const void *);

static int filed = -1; /* initialize to invalid */
static int eof;
static time_t opentime;

/* logging */
static int max_loglevel = LOG_WARNING;
#define LOG_EXIT	0x4000000

/* safeguard our LOG_EXIT extension */
#if (LOG_EXIT & (LOG_FACMASK | LOG_PRIMASK))
#error LOG_EXIT conflict
#endif

__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	static int logtostderr = -1;
	va_list va;
	char *str;
	int loglevel2 = loglevel & ~(LOG_FACMASK | LOG_PRIMASK);
	loglevel &= LOG_FACMASK | LOG_PRIMASK;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(NAME, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel, "%s", str);

	} else if (loglevel <= max_loglevel) {
		struct timespec tv;
		static char timbuf[64];

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
	}

	free(str);
	if (loglevel2 & LOG_EXIT)
		exit(1);
}
#define ESTR(x) strerror(x)

/* program */
static const char *const we_err[] = {
	[WRDE_BADCHAR] = "Illegal occurrence of newline or one of |, &, ;, <, >, (, ), {, }",
	[WRDE_BADVAL] = "An undefined shell variable was referenced, with WRDE_UNDEF",
	[WRDE_CMDSUB] = "Command substitution occurred with WRDE_NOCMD",
	[WRDE_NOSPACE] = "Out of memory",
	[WRDE_SYNTAX] = "Shell syntax error, such as unbalanced parentheses or unmatched quotes",
};

static int cmpfilename(const void *a, const void *b)
{
	const char **fa = (const char **)a;
	const char **fb = (const char **)b;
	return strverscmp(*fa, *fb) * -1;
}

static int cmpmtime(const void *a, const void *b)
{
	const char **fa = (const char **)a;
	const char **fb = (const char **)b;
	struct stat sta = {}, stb = {};

	stat(*fa, &sta);
	stat(*fb, &stb);
	return stb.st_mtime - sta.st_mtime;
}
static int cmpatime(const void *a, const void *b)
{
	const char **fa = (const char **)a;
	const char **fb = (const char **)b;
	struct stat sta = {}, stb = {};

	stat(*fa, &sta);
	stat(*fb, &stb);
	return stb.st_atime - sta.st_atime;
}

static const struct cmp {
	char name;
	int (*cmp)(const void *, const void *b);
} cmptable[] = {
	{ 'n', cmpfilename, },
	{ 'm', cmpmtime, },
	{ 'a', cmpatime, },

	{ 'f', cmpfilename, },
	{ 't', cmpmtime, },
};

static int rotate_file(const char *filename, time_t now)
{
	char *newfile;
	char tbuf[32];
	struct stat st;

	strftime(tbuf, sizeof(tbuf), "%Y%m%dT%H%M", localtime(&now));
	asprintf(&newfile, "%s-%s", filename, tbuf);
	if (stat(newfile, &st) >= 0) {
		/* file exists?
		 * append seconds */
		strftime(tbuf, sizeof(tbuf), "%Y%m%dT%H%M%S", localtime(&now));
		asprintf(&newfile, "%s-%s", filename, tbuf);
	}

	if (dryrun) {
		mylog(LOG_NOTICE, "$ mv %s %s ...", filename, newfile);

	} else if (rename(filename, newfile) < 0) {
		mylog(LOG_WARNING, "move %s to %s failed: %s", filename, newfile, ESTR(errno));
		free(newfile);
		/* nothing changed, return */
		return -1;
	} else {
		mylog(LOG_NOTICE, "%s rotated to %s", filename, newfile);
	}
	free(newfile);
	return 0;
}

static void vacuum_files(const char *pattern, time_t now)
{
	int ret, j;
	wordexp_t we = {};
	unsigned long csize;
	struct stat st;

	ret = wordexp(pattern, &we, WRDE_NOCMD | WRDE_REUSE);
	if (ret)
		mylog(LOG_ERR | LOG_EXIT, "'%s' failed: %s", pattern, we_err[ret] ?: "unknown error");
	if (we.we_wordc == 1 && stat(we.we_wordv[0], &(struct stat){}) != 0)
		/* no match */
		goto pttrn_done;
	qsort(we.we_wordv, we.we_wordc, sizeof(*we.we_wordv), cmpfile);
	/* sum all files */
	csize = 0;
	for (j = 0; j < we.we_wordc; ++j) {
		if (stat(we.we_wordv[j], &st) < 0) {
			if (errno == ENOENT)
				/* how can this happen? someone else deleted? */
				continue;
			mylog(LOG_ERR | LOG_EXIT, "stat %s: %s", we.we_wordv[j], ESTR(errno));
		}
		if ((maxaccsize && (csize > maxaccsize)) || (maxage && ((st.st_mtime + maxage) < now))) {
			if (dryrun)
				mylog(LOG_NOTICE, "$ rm %s ...", we.we_wordv[j]);
				/* nothing here */
			else if (unlink(we.we_wordv[j]) < 0)
				mylog(LOG_WARNING, "rm %s: %s", we.we_wordv[j], ESTR(errno));
			else
				mylog(LOG_NOTICE, "rm %s", we.we_wordv[j]);
			continue;
		}
		csize += st.st_size;
		mylog(LOG_INFO, "keep %s", we.we_wordv[j]);
	}
pttrn_done:
	wordfree(&we);
}

static int rotate_only(const char *filename, const char *filepattern)
{
	struct stat st;
	time_t now;
	int result = 0;

	time(&now);

	if (stat(filename, &st) < 0) {
		mylog(LOG_WARNING, "stat %s: %s", filename, ESTR(errno));
		return 0;
	}

	if ((maxdelay && (now - st.st_ctime) > maxdelay)
			|| (maxsize && st.st_size >= maxsize)) {
		if (rotate_file(filename, now) >= 0) {
			vacuum_files(filepattern, now);
			result = 1;
		}
	}
	return result;
}

static void rotate_now(const char *filename, const char *filepattern, int *pfd)
{
	time_t now;

	time(&now);

	if (rotate_file(filename, now) >= 0) {
		close(*pfd);
		*pfd = -1;
		vacuum_files(filepattern, now);
	}
}

static void stdin_handler(int fd, void *dat)
{
	static char buf[16384];
	int ret, buflen;
	struct stat st;

	ret = read(fd, buf, sizeof(buf));
	if (ret < 0 && errno == EAGAIN)
		return;
	if (ret < 0)
		mylog(LOG_ERR | LOG_EXIT, "read stdin: %s", ESTR(errno));
	buflen = ret;

	if (filed >= 0 && maxdelay && ((opentime + maxdelay) <= time(NULL)))
		/* rotate due to age of file */
		rotate_now(filename, filepattern, &filed);

	if (!buflen) {
		mylog(LOG_NOTICE, "stdin eof");
#if 0
		/* remove from libe event loop (if used) */
		libe_remove_fd(STDIN_FILENO);
#endif
		if (filed >= 0)
			/* final rotation on exit */
			rotate_now(filename, filepattern, &filed);
		eof = 1;
		return;
	}
	if (filed < 0) {
		filed = open(filename, O_WRONLY | O_APPEND | O_CREAT, 0666);
		if (filed < 0)
			mylog(LOG_ERR | LOG_EXIT, "open %s: %s", filename, ESTR(errno));
		mylog(LOG_INFO, "opened %s", filename);
		time(&opentime);
	}
	ret = write(filed, buf, buflen);
	if (ret < 0)
		mylog(LOG_ERR | LOG_EXIT, "write %s: %s", filename, ESTR(errno));
	if (ret < buflen)
		mylog(LOG_ERR | LOG_EXIT, "write %s incomplete: %u/%u", filename, ret, buflen);

	if (fstat(filed, &st) < 0)
		mylog(LOG_ERR | LOG_EXIT, "fstat %s: %s", filename, ESTR(errno));

	if (maxsize && (st.st_size >= maxsize))
		/* rotate due to size */
		rotate_now(filename, filepattern, &filed);
}

static double delay_strtod(const char *str, char **pendp)
{
	char *endp;
	double result;

	if (!pendp)
		pendp = &endp;
	result = strtod(str, pendp);
	switch (**pendp) {
	case 'y':
		result *= 365*24*60*60;
		++*endp;
		break;
	case 'w':
		result *= 7;
	case 'd':
		result *= 24;
	case 'h':
		result *= 60;
	case 'm':
		result *= 60;
	case 's':
		++*pendp;
		break;
	}
	return result;
}
static unsigned long size_strtoul(const char *str, char **pendp, int radix)
{
	char *endp;
	unsigned long result;

	if (!pendp)
		pendp = &endp;
	result = strtoul(str, pendp, radix);
	switch (**pendp) {
	case 'G':
		result *= 1024;
	case 'M':
		result *= 1024;
	case 'k':
	case 'K':
		result *= 1024;
		++*pendp;
		break;
	}
	return result;
}

int main(int argc, char *argv[])
{
	int ret, opt;
	int dostderr = 0;
	int remain_parent = 0;
	int j;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s (%s %s)\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case 'v':
		max_loglevel += 1;
		break;
	case 'f':
		filename = optarg;
		break;
	case 's':
		maxsize = size_strtoul(optarg, NULL, 0);
		break;
	case 'S':
		maxaccsize = size_strtoul(optarg, NULL, 0);
		break;
	case 't':
		maxdelay = delay_strtod(optarg, NULL);
		break;
	case 'T':
		maxage = delay_strtod(optarg, NULL);
		break;
	case '2':
		dostderr = 1;
		break;
	case 'p':
		remain_parent = 1;
		break;
	case 'L':
		logrotate = 1;
		break;
	case 'C':
		vacuum = 1;
		break;
	case 'c':
		cmpfile = NULL;
		for (j = 0; j < sizeof(cmptable)/sizeof(cmptable[0]); ++j) {
			if (cmptable[j].name == *optarg) {
				cmpfile = cmptable[j].cmp;
				break;
			}
		}
		if (!cmpfile)
			mylog(LOG_ERR, "--cmp=%s unknown", optarg);
		break;
	case 'n':
		dryrun = 1;
		break;
	default:
		fprintf(stderr, "unknown option '%c'", opt);
	case '?':
		fputs(help_msg, stderr);
		exit(1);
		break;
	}
	if (!cmpfile)
		cmpfile = cmptable[0].cmp;

	if (!maxsize && !maxdelay)
		mylog(LOG_ERR | LOG_EXIT, "Specify either -s or -t");

	if (logrotate) {
		int j;
		int result = 0;

		for (j = optind; j < argc; ++j) {
			asprintf(&filepattern, "%s-*", argv[j]);
			result += rotate_only(argv[j], filepattern);
			free(filepattern);
		}
		return !result;

	} else if (vacuum) {
		int j;
		time_t now;

		time(&now);

		for (j = optind; j < argc; ++j)
			vacuum_files(argv[j], now);
		/* return ok when files removed */
		return 0;
	}

	/* force no dry-run */
	dryrun = 0;

	asprintf(&filepattern, "%s-*", filename);

	/* spawn child process */
	if (optind < argc) {
		int pp[2];

		if (pipe(pp) < 0)
			mylog(LOG_EXIT | LOG_ERR, "pipe: %s", ESTR(errno));
		ret = fork();
		if (ret < 0)
			mylog(LOG_EXIT | LOG_ERR, "fork: %s", ESTR(errno));
		if ((ret > 0 && !remain_parent) || (!ret && remain_parent)) {
			/* parent, replace with target program
			 * This is less conventional,
			 * but makes brsimplelog a utility rather than
			 * the core tool.
			 * The process' exit code is that of the monitored program.
			 */
			if (dup2(pp[1], STDOUT_FILENO) < 0)
				mylog(LOG_EXIT | LOG_ERR, "dup2: %s", ESTR(errno));
			if (dostderr)
			if (dup2(pp[1], STDERR_FILENO) < 0)
				mylog(LOG_EXIT | LOG_ERR, "dup2: %s", ESTR(errno));
			close(pp[0]);
			close(pp[1]);
			execvp(argv[optind], argv+optind);
			mylog(LOG_EXIT | LOG_ERR, "execvp %s ...: %s", argv[optind], ESTR(errno));
		}
		if (dup2(pp[0], STDIN_FILENO) < 0)
			mylog(LOG_EXIT | LOG_ERR, "dup2: %s", ESTR(errno));
		close(pp[0]);
		close(pp[1]);
	}

	/* run */
	for (; !eof; )
		stdin_handler(STDIN_FILENO, NULL);

	return 0;
}
